
const { client, connect } = require('./api');
const csv = require('csvtojson');
const Aerospike = require('aerospike');
const uuid = require('uuid-random');

connect(async function (error) {
    if (error) {
        dbStatusCode = error.code
        console.log('Connection to Aerospike cluster failed!')
    } else {
        console.log('Connection to Aerospike cluster succeeded!');
        // execute once
        // await populateDb();
        // await createIndex();
        await registerModules();
        await getAllHistory("14787726", "2020-03-10", "2020-04-08");
        await getTotalPointsAmount('14787726', "2020-03-10", "2020-04-08");
    }
});

async function registerModules() {
    try {
        await Promise.all([
            client.udfRegister('./filter_by_date_range.lua'),
            client.udfRegister('./total_amount.lua')
        ]);
        console.log('Registered all modules');
    } catch(err) {
        console.log(err);
        return Promise.reject();
    }
}

async function getAllHistory(userId, fromDate, toDate) {
    let query = client.query('test', 'demo');
    query.where(Aerospike.filter.equal('user_id', userId));

    let fromDateEpoch = new Date(fromDate).getTime();
    let toDateEpoch = new Date(toDate).getTime();

    await query.apply('filter_by_date_range', 'filter_records', [ 'created_at', fromDateEpoch, toDateEpoch])

    var records = [];
    var stream = query.foreach()
    stream.on('data', function (record) {
        records.push(record.bins)
    });
    stream.on('error', function (error) {
        console.log(error);
    });
    stream.on('end', function () {
        console.log("Total records received: ", records.length);
        // console.log(records)
    });
}

async function getTotalPointsAmount(userId, fromDate, toDate) {
    let query = client.query('test', 'demo');
    query.where(Aerospike.filter.equal('user_id', userId));

    let fromDateEpoch = new Date(fromDate).getTime();
    let toDateEpoch = new Date(toDate).getTime();

    let res = await query.apply('total_amount', 'sum_points', [ 'created_at', fromDateEpoch, toDateEpoch]);
    console.log(res);
}

async function populateDb() {
    let jsonArray = await csv().fromFile('./points_hist.csv');

    let promises = jsonArray.map(async (doc) => {
        doc.created_at = new Date(doc.created_at).getTime();
        doc.expires_on = new Date(doc.expires_on).getTime();
        doc.updated_at = new Date(doc.updated_at).getTime();

        try {
            let key = new Aerospike.Key('test', 'demo', uuid());
            return client.put(key, doc).catch(() => "failed aaa");
        } catch (error) {
            console.error('Error:', error)
            process.exit(1)
        }
    });
    return await Promise.all(promises)
    .then(() => console.log("Added records count: ",jsonArray.length))
}

async function createIndex() {
    let options = {
        ns: 'test',
        set: 'demo',
        bin: 'user_id',
        index: 'user_id_index',
        datatype: Aerospike.indexDataType.STRING
    };

    return new Promise((resolve, reject) => {
            client.createIndex(options, function (err, job) {
            if (err) {
                console.log("Failed to create index: ", err);
                return reject();
            }
            job.waitUntilDone(function (error) {
                resolve();
                console.log('Index created successfully.');
            })
        })
    });
}