
const { aerospikeConfig, aerospikeDBParams } = require('./db-connection');
const Aerospike = require('aerospike')

const client = Aerospike.client(aerospikeConfig)
// Establish connection to the cluster
exports.connect = function (callback) {
    client.connect(callback)
}
// Write a record
exports.writeRecord = function (k, v, callback) {
    let key = new Aerospike.Key(aerospikeDBParams().defaultNamespace, aerospikeDBParams().defaultSet, k)
    client.put(key, v, function (error) {
        if (error) {
            return callback(error)
        } else {
            return callback(null, 'ok')
        }
    })
}
// Read a record
exports.readRecord = function (k, callback) {
    let key = new Aerospike.Key(aerospikeDBParams().defaultNamespace, aerospikeDBParams().defaultSet, k)
    client.get(key, function (error, record) {
        if (error) {
            return callback(error)
        } else {
            let bins = record.bins
            return callback(null, k + ' ' + bins.greet)
        }
    })
}

exports.client = client;