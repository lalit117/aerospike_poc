// const express = require('express')
// const http = require('http')
const uuid = require('uuid-random');

const api = require('./api')
let dbStatusCode = 0

let jsonArray
api.connect(async function (error) {
    if (error) {
        dbStatusCode = error.code
        console.log('Connection to Aerospike cluster failed!')
    } else {
        // await populateDb()
        // await createIndex()
        await query()
        // await registerModule()
        console.log('Connection to Aerospike cluster succeeded!')
    }
})
const Aerospike = require('aerospike')
async function query() {
    let query = api.client.query('test', 'demo')
    // new Aerospike.Key('test', 'demo', 'r2' )
    // query.where(Aerospike.filter.range('created_at', 0, 15834949991132))
    // query.where(Aerospike.filter.range('i', 0, 100))
    query.where(Aerospike.filter.equal('user_id', '7'));
    // query.where(Aerospike.filter.range('created_at',
    // // new Date("2020-03-13T12:34:28.317Z").getTime(), 
    // 1584433113163,
    //            Date.now()
    // ));

    query.apply('filtrec', 'str_between', [ 'created_at', 1583840599113, 1584013399113] ,function (error, result) {
        if (!error) {
            console.log('sccess:', result)
        }
            // console.log(result)
    })

    // query.apply('filtrec', 'str_between', [ 'i', 0, 100] ,function (error, result) {
    //     if (!error) {
    //         console.log(result)
    //     }
    //         console.log(result)
    // })

    // query.background('count', 'count', function (error, job) {
    //     if (error) throw error
    //     job.waitUntilDone(function (error) {
    //         console.log("dkjkldcdc");
    //       // background job has completed
    //     });
    //   });

    console.log("querying: ")
    var records = [];
    var stream = query.foreach()
    stream.on('data', function (record) {
        // console.log(record);
        records.push(record)
    })
    stream.on('error', function (error) {
        console.log(error);
      //handle error
    })
    stream.on('end', function () {
        console.log(records.length)
      //signal the end of query result
    })
}

async function populateDb() {
    jsonArray = [
        {
            "_id" : "5e6b71d745f3af0db008cfa8",
            "application_id" : "000000000000000000000001",
            "user_id" : "7",
            "points" : 800.0,
            "remaining_points" : 800.0,
            "txn_name" : "credit_code_redeemed",
            "expires_on" : new Date("2020-03-14T11:43:19.113Z").getTime(),
            "meta" : {
                "referrer_user_id" : "48"
            },
            "created_at" : new Date("2020-03-14T12:40:19.113Z").getTime(),
            "updated_at" : new Date("2020-03-14T11:43:19.113Z").getTime()
        }
    ]

        console.log('writing record')
        api.writeRecord(uuid(), jsonArray[0], function (error, result) {
            if (error) {
                // handle failure
                console.log("ddddddddd: ",error.message)
            } else {
                // handle success
            }
        })
}

function createIndex() {
    var options = {
        ns: 'test',
        set: 'demo',
        bin: 'created_at',
        index: 'index_on_created_at',
        datatype: Aerospike.indexDataType.NUMERIC
    }

    api.client.createIndex(options, function (error, job) {
        if (error) {
            // error creating index
        }
        job.waitUntilDone(function (error) {
            console.log('Index was created successfully.')
        })
    })
}

function registerModule() {

    api.client.udfRegister('./count.lua', function (error) {
        if (!error) {
        // registration successfull
        }
        console.log("module registred");
    })
}